#include "XbusMessage.h"
#include "xbusdef.h"
#include "xbusutility.h"
#include <XsensConfig.h>

uint16_t XsDataIdentifier(XsDataIdentifierId id, XsCoordinateFrame frame, XsPrecision precision) {

	return 0; 
};


XbusMessage::XbusMessage() {
mid = (XsMessageId) 0;
}


XbusMessage::XbusMessage(XsMessageId id): mid(id) {
	length = 0; 
}


XbusMessage::XbusMessage(XsMessageId id, uint8_t *data_in, uint8_t length_in): 
mid(id), length(length_in) {
	uint8_t * rawData = data; 
	for (int i=0; i<length_in; i++) {
 		rawData = XbusUtility_writeU8(rawData, data_in[i]);
	}
}


/*!
* Length_in: number of elements in the data array
*/ 
XbusMessage::XbusMessage(XsMessageId id, uint16_t* data_in, uint16_t length_in):
 mid(id), length(length_in*2)  {

	uint8_t * rawData = data; 
	for (int i=0; i<length_in; i++) {
 		rawData = XbusUtility_writeU16(rawData, *data_in);
	}
}

XbusMessage::XbusMessage(XsMessageId id, OutputConfiguration* data_in, const uint16_t length_in):
 mid(id), length(length_in)  {

	uint8_t * rawData = data; 
	for (int i=0; i<length_in; i++) {
 		rawData = XbusUtility_writeU16(rawData, data_in[i].dtype);
		rawData = XbusUtility_writeU16(rawData, data_in[i].freq);
	}
}

/*!
* Length_in: number of elements in the data array
*/ 

XbusMessage::XbusMessage(XsMessageId id, uint32_t* data_in, uint16_t length_in):
 mid(id), length(length_in*4)  {

 	uint8_t * rawData = data; 
	for (int i=0; i<length_in; i++) {
 		rawData =  	XbusUtility_writeU32(rawData, *data_in);
	}

 }

/*!
 * \brief Get a string description for the passed data identifier.
 */
char const* XbusMessage::dataDescription(uint16_t id)
{
	switch (id)
	{
		case XDI_EulerAngles:
			return "Euler Angles";
			
		case XDI_PacketCounter:
			return "Packet counter";

		case XDI_SampleTimeFine:
			return "Sample time fine";

		case XDI_Quaternion:
			return "Quaternion";

		case XDI_DeltaV:
			return "Velocity increment";

		case XDI_Acceleration:
			return "Acceleration";

		case XDI_RateOfTurn:
			return "Rate of turn";

		case XDI_DeltaQ:
			return "Orientation increment";

		case XDI_MagneticField:
			return "Magnetic field";

		case XDI_StatusWord:
			return "Status word";

		case XDI_Temperature:
			return "Temperature:";
		default:
			return "Unknown data type";
	}
}

/*!
 * \brief Get a data item from an XMID_MtData2 Xbus message.
 * \param item Pointer to where to store the data.
 * \param id The data identifier to get.
 * \param message The message to read the data item from.
 * \returns true if the data item is found in the message, else false.
 */
bool XbusMessage::getDataItem(void* item, uint16_t  id)
{
	uint8_t const* raw = getPointerToData(id, data, length);
	if (raw)
	{
		switch (id)
		{
			case XDI_PacketCounter:
				raw = XbusUtility_readU16((uint16_t*) item, raw);
				break;

			case XDI_SampleTimeFine:
				break;
			case XDI_StatusWord:
				raw = XbusUtility_readU32((uint32_t*) item, raw);
				break;

			case XDI_Quaternion:
				readFloats((float *) item, raw, 4);
				break;
			case XDI_DeltaQ:
				readFloats((float *)item, raw, 4);
				break;

			case XDI_DeltaV:
				break;
			case XDI_Acceleration:
				readFloats((float *)item, raw, 3);
				break;
			case XDI_RateOfTurn:
				readFloats((float *)item, raw, 3);
				break;
			case XDI_MagneticField:
				readFloats((float *) item, raw, 3);
				break;
			case XDI_Temperature:
				readFloats((float *) item, raw, 1);
				break;
			case XDI_EulerAngles:
				readFloats((float *) item, raw, 3);
				break;
			default:
				return false;
		}
		return true;
	}
	else
	{
		return false;
	}
}




/*!
 * \brief Get a pointer to the data corresponding to \a id.
 * \param id The data identifier to find in the message.
 * \param data Pointer to the raw message payload.
 * \param dataLength The length of the payload in bytes.
 * \returns Pointer to data item, or NULL if the identifier is not present in
 * the message.
 */
uint8_t const* XbusMessage::getPointerToData(uint16_t  id, uint8_t const* data, uint16_t dataLength)
{
	uint8_t const* dptr = data;
	while (dptr < data + dataLength)
	{
		uint16_t itemId;
		uint8_t itemSize;
		dptr = XbusUtility_readU16(&itemId, dptr);
		dptr = XbusUtility_readU8(&itemSize, dptr);

		if (id == itemId)
			return dptr;

		dptr += itemSize;
	}
	return NULL;
}

/*!
 * \brief Format a message into the raw Xbus format ready for transmission to
 * a motion tracker.
 */
size_t XbusMessage::format(uint8_t* raw,  enum XbusLowLevelFormat format) const
{
	uint8_t* dptr = raw;
	switch (format)
	{
		case XLLF_I2c:
			{
				*dptr++ = XBUS_CONTROL_PIPE;
			}
			break;

		case XLLF_Spi:
			{
				*dptr++ = XBUS_CONTROL_PIPE;
				// Fill bytes required to allow MT to process data
				*dptr++ = 0;
				*dptr++ = 0;
				*dptr++ = 0;
			}
			break;

		case XLLF_Uart:
			{
				*dptr++ = XBUS_PREAMBLE;
				*dptr++ = XBUS_MASTERDEVICE;
			}
			break;
	}

	uint8_t checksum = (uint8_t)(-XBUS_MASTERDEVICE);

	*dptr = mid;
	checksum -= *dptr++;

	uint16_t length = messageLength();

	if (length < XBUS_EXTENDED_LENGTH)
	{
		*dptr = length;
		checksum -= *dptr++;
	}
	else
	{
		*dptr = XBUS_EXTENDED_LENGTH;
		checksum -= *dptr++;
		*dptr = length >> 8;
		checksum -= *dptr++;
		*dptr = length & 0xFF;
		checksum -= *dptr++;
	}

	formatPayload(dptr);
	for (int i = 0; i < length; ++i)
	{
		checksum -= *dptr++;
	}
	*dptr++ = checksum;

	return dptr - raw;
}

/*!
 * \brief Format the payload of a message from a native data type to
 * raw bytes.
 */
void XbusMessage::formatPayload(uint8_t* raw) const
{
	switch (mid)
	{
		case XMID_SetOutputConfig:
			formatOutputConfig(raw);
			break;

		default:
			for (int i = 0; i < length; ++i)
			{
				*raw++ = ((uint8_t*) data)[i];
			}
			break;
	}
}

/*!
 * \brief Format a message with a pointer to an array of OutputConfiguration elements.
 */
void XbusMessage::formatOutputConfig(uint8_t* raw) const
{
	const uint8_t* rawInputData = data; 

	for (int i = 0; i < length; ++i)
	{
		uint16_t dtype, freq;
		rawInputData = XbusUtility_readU16(&dtype, rawInputData);
		rawInputData = XbusUtility_readU16(&freq, rawInputData);
		DEBUG_MESSAGE("Formatting output conf --> Type: 0x%x, Freq: 0x%x \n\r", dtype, freq);

		raw = XbusUtility_writeU16(raw, dtype);
		raw = XbusUtility_writeU16(raw, freq);
	}
}

/*!
 * \brief Calculate the number of bytes needed for \a message payload.
 */
uint16_t XbusMessage::messageLength() const
{
	switch (mid)
	{
		case XMID_SetOutputConfig:
			return length * 2 * sizeof(uint16_t);

		default:
			return length;
	}
}

