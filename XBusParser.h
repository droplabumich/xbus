#ifndef __XBUSPARSER_H
#define __XBUSPARSER_H

#include <stddef.h>
#include <stdint.h>
#include "XbusMessage.h"

/*! \brief XbusParser states. */
enum XbusParserState
{
	XBPS_Preamble,          /*!< \brief Looking for preamble. */
	XBPS_BusId,             /*!< \brief Waiting for bus ID. */
	XBPS_MessageId,         /*!< \brief Waiting for message ID. */
	XBPS_Length,            /*!< \brief Waiting for length. */
	XBPS_ExtendedLengthMsb, /*!< \brief Waiting for extended length MSB*/
	XBPS_ExtendedLengthLsb, /*!< \brief Waiting for extended length LSB*/
	XBPS_Payload,           /*!< \brief Reading payload. */
	XBPS_Checksum           /*!< \brief Waiting for checksum. */
};



class XbusParser
{
public:

	XbusParser();
 	void parseU32(uint8_t const* rawData);
	void parseByte(const uint8_t byte);
	void prepareForPayload();
	void parseMessagePayload();
	void parseOutputConfig(uint8_t const* rawData);
	XbusParserState getState();

	virtual void handleMessage() = 0;

	/*! \brief Callbacks for memory management, and message handling. */
	//struct XbusParserCallback callbacks;
	/*! \brief Storage for the current message being received. */
	XbusMessage currentMessage;
	/*! \brief The number of bytes of payload received for the current message. */
	uint16_t payloadReceived;
	/*! \brief The calculated checksum for the current message. */
	uint8_t checksum;
	/*! \brief The state of the parser. */
	XbusParserState state;
};


#endif