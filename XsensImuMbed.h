#ifndef __XSENSIMUMBED_H
#define __XSENSIMUMBED_H

#include "mbed.h"
#include "XsensImu.h"
#include "trace_debug.h"

#define gotResponseFlag (1UL << 0)

class XsensImuMbed: public XsensImu  
{
public:

	XsensImuMbed(RawSerial* iface);

	bool sendMessage(XbusMessage const *m); 

	void handleMessage();

	XbusMessage doTransaction(XbusMessage const *m, uint32_t timeout=50);

public:
	float euler[3], accel[3], gyro[3];
	float temperature, timestamp;
	bool newData;
	uint32_t status_word; 
private: 
	RawSerial* serial; 

	EventFlags event_flags;
	CircularBuffer<XbusMessage, 3> responseBuffer;

	//Queue<XbusMessage, 1> responseQueue;
	//MemoryPool<XbusMessage, 1> responseMemoryPool;

};


#endif