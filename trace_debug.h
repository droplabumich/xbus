
#ifndef _TRACEDEBUG_H_
#define _TRACEDEBUG_H_
// -- within a header file named something like trace.h --
enum {
	TRACE_LEVEL_WARNING,
	TRACE_LEVEL_DEBUG
};
/* each time we compile or run the program,
* we determine what the trace level is.
* this parameter is available to the macros
* without being explicitly passed to them*/

//#define DEBUG_TEST


// Our first macro prints if the trace level we selected
// is TRACE_LEVEL_DEBUG or above.
// The traceLevel is used in the condition
// and the regular parameters are used in the action that follows the IF
#ifdef DEBUG_TEST
#include "mbed.h"
#define DEBUG_MESSAGE(...) printf(__VA_ARGS__)
#else
#define DEBUG_MESSAGE(...)
#endif

#endif