#include "XsensImuMbed.h"
#include "trace_debug.h"

XsensImuMbed::XsensImuMbed(RawSerial* iface): serial(iface) {

}

bool XsensImuMbed::sendMessage(XbusMessage const *m) {
	uint8_t buf[64];
  	size_t rawLength = m->format(buf, XLLF_Uart);
	//DEBUG_MESSAGE("Sending message with id 0x%x and length %i\n\r",m->mid, rawLength);
	if (rawLength>64) {
		DEBUG_MESSAGE("Error! Tried to send a message with length %i bytes, while max is 64bytes ", rawLength);
		return false;
	}
  	for (size_t i = 0; i < rawLength; ++i) {
    	serial->putc(buf[i]);
		DEBUG_MESSAGE("%02x", buf[i]);
  	}
	DEBUG_MESSAGE("\n\r");
	return true;
} 


void XsensImuMbed::handleMessage(){

	if (currentMessage.mid == XMID_MtData2) {
		newData = true;
    	if (currentMessage.getDataItem(euler, XDI_EulerAngles)) {
    		DEBUG_MESSAGE("Roll: %5.2f, Pitch: %5.2f, Yaw: %5.2f, ", euler[0], euler[1], euler[2]);
    	}
    	if (currentMessage.getDataItem(accel, XDI_Acceleration)) {
    		DEBUG_MESSAGE("AccX: %5.2f, AccY: %5.2f, AccZ: %5.2f, ", accel[0], accel[1], accel[2]);
    	}
		if (currentMessage.getDataItem(gyro, XDI_RateOfTurn)) {
    		DEBUG_MESSAGE("Roll: %5.2f, Pitch: %5.2f, Yaw: %5.2f, ", gyro[0], gyro[1], gyro[2]);
    	}
    	if (currentMessage.getDataItem(&status_word, XDI_StatusWord)) {
    		DEBUG_MESSAGE("Status byte: %x ", status_word);
    	}
    	DEBUG_MESSAGE("\n\r");
	} else {
		responseBuffer.push(currentMessage);
		//DEBUG_MESSAGE("Got Response with mid 0x%x\n\r",currentMessage.mid);
		event_flags.set(gotResponseFlag);
	}
	

}

XbusMessage XsensImuMbed::doTransaction(XbusMessage const *m, uint32_t timeout) {
	sendMessage(m);
	
	// Receive stuff goes here
	if (event_flags.wait_all(gotResponseFlag, timeout) == gotResponseFlag) {
		XbusMessage response; 
		if (responseBuffer.pop(response)) {
			//DEBUG_MESSAGE("doTransaction Finished\n\r");
			return response;
		} else {
			DEBUG_MESSAGE("Got flag but could not pop from buffer.\n\r");
		}
	}
	else {
		return XbusMessage();
	}
	//return true;
}