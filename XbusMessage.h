#ifndef __XBUSMESSAGE_H
#define __XBUSMESSAGE_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include "trace_debug.h"

#define MAX_XBUS_DATA_LENGTH 128

struct OutputConfiguration; //Forward declaration of outputconfiguration

/*! \brief Xbus message IDs. */
enum XsMessageId
{
	XMID_Wakeup             = 0x3E,
	XMID_WakeupAck          = 0x3F,
	XMID_ReqDid             = 0x00,
	XMID_DeviceId           = 0x01,
	XMID_GotoConfig         = 0x30,
	XMID_GotoConfigAck      = 0x31,
	XMID_GotoMeasurement    = 0x10,
	XMID_GotoMeasurementAck = 0x11,
	XMID_MtData2            = 0x36,
	XMID_ReqOutputConfig    = 0xC0,
	XMID_SetOutputConfig    = 0xC0,
	XMID_ReqOutputConfigAck = 0xC1,
	XMID_SetOutputConfigAck = 0xC1,
	XMID_Reset              = 0x40,
	XMID_ResetAck           = 0x41,
	XMID_Error              = 0x42,
	XMID_ReqFWRev			= 0x12,
	XMID_FirmwareRev		= 0x13,
	XMID_RunSelfTest		= 0x24,
	XMID_RunSelfTestResult  = 0x25, 
	XMID_SetNoRotation 		= 0x22, 
	XMID_SetNoRotationAck 	= 0x23,
	XMID_ReqOptionFlags		= 0x48,
	XMID_ReqOptionFlagsAck  = 0x49, 
	XMID_SetOptionFlags		= 0x48, 
	XMID_SetOptionFlagsAck	= 0x49,
	XMID_ReqFilterProfile   = 0x64,
	XMID_SetFilterProfile   = 0x64, 
	XMID_ReqFilterProfileAck= 0x65,
};

/*! \brief Xbus data message type IDs. */
enum XsDataIdentifierId
{
	XDI_Temperature    = 0x0810,
	XDI_UtcTime		   = 0x1010,
	XDI_PacketCounter  = 0x1020,
	XDI_SampleTimeFine = 0x1060,
	XDI_SampleTimeCoarse = 0x1070,

	XDI_Quaternion     = 0x2010,
	XDI_RotationMatrix = 0x2020, 
	XDI_EulerAngles	   = 0x2030,

	XDI_BaroPressure   = 0x3010,

	XDI_DeltaV         = 0x4010,
	XDI_Acceleration   = 0x4020,
	XDI_FreeAcceleration = 0x4030, 
	XDI_AccelerationHR = 0x4040, 

	XDI_AltitudeEllipsoid = 0x5020, 
	XDI_PositionEcef	  = 0x5030, 
	XDI_LatLon 			  = 0x5040, 

	XDI_GnssPvtData 	 = 0x7010, 
	XDI_GnssSatInfo		 = 0x7020, 

	XDI_RateOfTurn     = 0x8020,
	XDI_DeltaQ         = 0x8030,
	XDI_RateOfTurnHR   = 0x8040, 

	XDI_RawAccGyrMagTemp = 0xA010, 
	XDI_RawGyroTemp	     = 0xA020, 

	XDI_MagneticField  = 0xC020,

	XDI_VelocityXYZ		= 0xD010, 

	XDI_StatusByte 	   = 0xE010, 
	XDI_StatusWord     = 0xE020,
};


enum XsCoordinateFrame {
	XDI_ENU = 0x0,
	XDI_NED = 0x4, 
	XDI_NWU = 0x8,
};

enum XsPrecision {
	XDI_S32 = 0x0, 
	XDI_FP32 = 0x1, 
	XDI_FP48 = 0x2, 
	XDI_D64 = 0x3, 
};

uint16_t XsDataIdentifier(XsDataIdentifierId id, XsCoordinateFrame frame, XsPrecision precision); 

struct ValueStringTuple {
    int value;
    char const * name;
};


ValueStringTuple const XsSelfTestResults[] = {
	{0, "X Axis Accelerometer"},
	{1, "Y Axis Accelerometer"},
	{2, "Z Axis Accelerometer"},
	{3, "X Axis Gyroscope"},
	{4, "Y Axis Gyroscope"},
	{5, "Z Axis Gyroscope"},
	{6, "X Axis Magnetometer"},
	{7, "Y Axis Magnetometer"},
	{8, "Z Axis Magnetometer"},
};




/*!
 * \brief Low level format to use when formating Xbus messages for transmission.
 */
enum XbusLowLevelFormat
{
	/*! \brief Format for use with I2C interface. */
	XLLF_I2c,
	/*! \brief Format for use with SPI interface. */
	XLLF_Spi,
	/*! \brief Format for use with UART interface. */
	XLLF_Uart
};



class  XbusMessage
{
public:

	XbusMessage();


	XbusMessage(XsMessageId id);
	XbusMessage(XsMessageId id, uint8_t *data_in, uint8_t length_in);
	XbusMessage(XsMessageId id, uint16_t *data_in, uint16_t length_in);
	XbusMessage(XsMessageId id, uint32_t* data_in, uint16_t length_in);
	XbusMessage(XsMessageId id, OutputConfiguration* data_in, const uint16_t length_in);
	/*! \brief The message ID of the message. */
	XsMessageId mid;


	size_t format(uint8_t* raw, enum XbusLowLevelFormat format) const;
	void formatPayload(uint8_t* raw) const;
	void formatOutputConfig(uint8_t* raw) const;
	uint16_t messageLength() const;

	bool getDataItem(void* item, uint16_t id);
	char const* dataDescription(uint16_t id);
	uint8_t const* getPointerToData(uint16_t id, uint8_t const* data, uint16_t dataLength);


	/*!
	 * \brief The length of the payload.
	 *
	 * \note The meaning of the length is message dependent. For example,
	 * for XMID_OutputConfig messages it is the number of OutputConfiguration
	 * elements in the configuration array.
	 */
	uint16_t length;
	/*! \brief Pointer to the payload data. */
	uint8_t data[MAX_XBUS_DATA_LENGTH];

};

#endif


