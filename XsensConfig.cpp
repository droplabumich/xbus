#include "XsensConfig.h"

ImuOptions::ImuOptions() {
	disableAutoStore = false; 
	disableAutoMeasurement = false; 
	enableBeidou = false; 
	enableAHS = false; 
	enableConfigurableBusId = false; 
	enableInRunCompassCalibration = false; 
 	attitude_rate = 0;
 	acceleration_rate = 0;
 	rateofturn_rate = 0;
 	temperature_rate = 0;
};


ImuOptions::ImuOptions(bool disAutoStore, bool disAutoMeas, bool enbleBeidou, 
	bool enbleAHS, bool enbleConfBusID, bool IRCC, 
	uint16_t attitude_rate, uint16_t acceleration_rate, 
	uint16_t rateofturn_rate, uint16_t temperature_rate, XsFilterId filter): disableAutoStore(disAutoMeas), 
	disableAutoMeasurement(disAutoMeas), enableBeidou(enbleBeidou), enableAHS(enbleAHS), 
	enableConfigurableBusId(enbleConfBusID), enableInRunCompassCalibration(IRCC), 
	attitude_rate(attitude_rate), acceleration_rate(acceleration_rate), rateofturn_rate(rateofturn_rate), 
	 temperature_rate(temperature_rate), _filter(filter) {

}

ImuOptions::ImuOptions(uint32_t flags, uint16_t attitude_rate, uint16_t acceleration_rate, 
	uint16_t rateofturn_rate, uint16_t temperature_rate, XsFilterId filter): attitude_rate(attitude_rate),
	 acceleration_rate(acceleration_rate), rateofturn_rate(rateofturn_rate), 
	 temperature_rate(temperature_rate), _filter(filter) {

	if (bitSetU32(flags, DisableAutoStoreFlag))
		disableAutoStore = true; 
	else 
		disableAutoStore = false; 

	if (bitSetU32(flags, DisableAutoMeasurement))
		disableAutoMeasurement=true; 
	else 
		disableAutoMeasurement = false; 
	
	if (bitSetU32(flags, EnableBeidou))
		enableBeidou = true; 
	else
		enableBeidou = false; 

	if (bitSetU32(flags, EnableAHS))
		enableAHS = true; 
	else 
		enableAHS = false; 

	if (bitSetU32(flags, EnableConfigurableBusId))
		enableConfigurableBusId = true; 
	else
		enableConfigurableBusId = false; 

	if (bitSetU32(flags, EnableInRunCompassCalibration)) 
		enableInRunCompassCalibration = true; 
	else 
		enableInRunCompassCalibration = false; 

}

uint32_t ImuOptions::toFlags() {
	uint32_t flags = 0; 

	if (disableAutoStore) 
		flags = flags | (1<<DisableAutoStoreFlag);
	if (disableAutoMeasurement)
		flags = flags | (1<< DisableAutoMeasurement);
	if (enableBeidou) 
		flags = flags | (1<< EnableBeidou);
	if (enableAHS) 
		flags = flags | (1<< EnableAHS);
	if (enableConfigurableBusId) 
		flags = flags | (1<< EnableConfigurableBusId);
	if (enableInRunCompassCalibration) 
		flags = flags | (1<< EnableInRunCompassCalibration);

	return flags; 

}

uint32_t ImuOptions::getClearFlags(uint32_t* setFlags) {
	uint32_t clearFlags = 0; 
	if (!disableAutoStore && bitSetU32(*setFlags, DisableAutoStoreFlag)) {
		clearFlags = clearFlags | (1<<DisableAutoStoreFlag);
	}
	if (!disableAutoMeasurement && bitSetU32(*setFlags, DisableAutoMeasurement)) {
		clearFlags = clearFlags | (1<<DisableAutoMeasurement);
	}
	if (!enableBeidou && bitSetU32(*setFlags, EnableBeidou)) {
		clearFlags = clearFlags | (1<<EnableBeidou);
	}
	if (!enableAHS && bitSetU32(*setFlags, EnableAHS)) {
		clearFlags = clearFlags | (1<<EnableAHS);
	}
	if (!enableConfigurableBusId && bitSetU32(*setFlags, EnableConfigurableBusId)) {
		clearFlags = clearFlags | (1<<EnableConfigurableBusId);		
	}
	if (!enableInRunCompassCalibration && bitSetU32(*setFlags, EnableInRunCompassCalibration)) {
		clearFlags = clearFlags | (1<<EnableInRunCompassCalibration);		
	}

	return clearFlags;
}

bool ImuOptions::getOutputConfig(OutputConfiguration* outputConfig, const uint8_t* const length) {
	if (*length < 4) {
		return false;
	}
	else {
		outputConfig[0].dtype = XDI_EulerAngles;
		outputConfig[1].dtype = XDI_Acceleration;
		outputConfig[2].dtype = XDI_RateOfTurn;
		outputConfig[3].dtype = XDI_Temperature;
		outputConfig[0].freq = attitude_rate;
		outputConfig[1].freq = acceleration_rate;
		outputConfig[2].freq = rateofturn_rate;
		outputConfig[3].freq = temperature_rate;
		return true;
	}
}



