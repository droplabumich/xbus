#ifndef __XSENSIMU_H
#define __XSENSIMU_H

#include "XBusParser.h"
#include "XsensConfig.h"

enum DriverState {
	configuration, 
	stream,
	unknown,
};

class XsensImu: public XbusParser
{
public:

	XsensImu();

	bool initialize(); 
	bool reset();
	bool configure(ImuOptions *options);
	bool streamMeasurements();

	bool getID(uint32_t &id);
	bool enterconfig(); 
	bool getFirmwareRev(uint8_t &fw_major, uint8_t &fw_minor, uint8_t &fw_rev);
	bool getOutputConfig(uint32_t outputsettings);
	bool setOutputConfig(OutputConfiguration* outputsettings, const uint8_t* length);
	bool runSelfTest();
	bool manualGyroBiasEstimation(uint16_t* seconds);
	bool getOptionFlags(uint32_t *optionFlags); 
	bool setOptionFlags(uint32_t *setFlags, uint32_t *clearFlags);
	
	bool getFilterProfile(uint8_t* filter, uint8_t* version);
	bool setFilterProfile(uint8_t* filter);

	void handleError(XbusMessage* error_msg);

	virtual bool sendMessage(XbusMessage const *m) = 0;
	virtual XbusMessage doTransaction(XbusMessage const *m, uint32_t timeout=10) = 0;

public:
	uint32_t deviceId;

private:
	DriverState state; ///< Imu driver state machine variable 

	uint8_t fw_major, fw_minor, fw_rev; ///< Firmware version 

	uint32_t  productCode; ///< Device identification 

	uint8_t frequency; 




};




#endif