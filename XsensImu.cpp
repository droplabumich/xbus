#include "XsensImu.h"
#include "trace_debug.h"
#include "xbusutility.h"

XsensImu::XsensImu() {
	state = unknown;
}

bool XsensImu::initialize() {
	bool result = enterconfig(); 
	result = result && getID(deviceId);
	result = result && getFirmwareRev(fw_major, fw_minor, fw_rev); 
	result = result && runSelfTest();
	return result; 
}

bool XsensImu::configure(ImuOptions *options) {
	
	// Set option flags 
	uint32_t currentflags = 0;
	bool flags_set = getOptionFlags(&currentflags);
	uint32_t flags2set = options->toFlags();
	uint32_t flags2clear = options->getClearFlags(&currentflags);
	flags_set = flags_set && setOptionFlags(&flags2set, &flags2clear);

	// Set filter options
	uint8_t filter = 0, version=0;
	uint8_t config_filter = options->getFilter(); 
	bool filter_set = setFilterProfile(&config_filter);
	filter_set = filter_set && getFilterProfile(&filter, &version);
	filter_set = filter_set && (filter==config_filter);
	
	// Set output options
	const uint8_t length = 4;
	OutputConfiguration outputConfig[length];
	options->getOutputConfig(outputConfig, &length);
	bool outputconfig_set = setOutputConfig(outputConfig, &length);

	return flags_set && outputconfig_set && filter_set;
}

bool XsensImu::reset() {
	bool result = initialize();
	result = result && streamMeasurements();
	return result;
}

void XsensImu::handleError(XbusMessage* error_msg) {
	uint8_t const* rawData = error_msg->data;
	uint8_t error_code = 0; 
	rawData = XbusUtility_readU8(&error_code, rawData);

	DEBUG_MESSAGE("Got error message with error code %i. \n\r ", error_code);


}

bool XsensImu::getID(uint32_t &id) {
    if (!enterconfig()) {
    	return false; 
    }

    XbusMessage reqDid = XbusMessage(XMID_ReqDid);
  	XbusMessage response = doTransaction(&reqDid);

  	id = 0;
  	if (response.mid == XMID_DeviceId) {
     	id = *(uint32_t *) response.data;
			deviceId = id;
     	DEBUG_MESSAGE("Got device id 0x%x \n\r", id);
     	
     	return true; 
    } else {
    	return false; 
    }
}

bool XsensImu::getFirmwareRev(uint8_t &fw_maj, uint8_t &fw_min, uint8_t &fw_revi) {
	XbusMessage getFwVersion = XbusMessage(XMID_ReqFWRev);
	XbusMessage result = doTransaction(&getFwVersion);
	if (result.mid == XMID_FirmwareRev) {
		fw_maj = result.data[0];
		fw_min = result.data[1]; 
		fw_revi	 = result.data[2];
		DEBUG_MESSAGE("Imu firmware version %i.%i.%i \n\r", fw_maj, fw_min, fw_revi);
		DEBUG_MESSAGE("Response length %i \n\r", result.length);

		return true; 
	} 
	
	return false; 
	
}

bool XsensImu::runSelfTest() {
	XbusMessage runTest = XbusMessage(XMID_RunSelfTest);
	XbusMessage response = doTransaction(&runTest, (uint32_t)1000);
	if (response.mid == XMID_RunSelfTestResult) {
		bool result = true;
		uint16_t selftestresult;
		XbusUtility_readU16(&selftestresult, response.data);
		
		for (int i=0; i<9; i++) {
     	if(!bitSetU16(selftestresult,XsSelfTestResults[i].value)) {
				DEBUG_MESSAGE("%s failed self test\n\r", XsSelfTestResults[i].name);
				result = false;
			} else {
				DEBUG_MESSAGE("%s passed self test\n\r", XsSelfTestResults[i].name);
			}
     	}

     	return result; 
	} else {
		DEBUG_MESSAGE("No seltTest ack received. Got: 0x%x \n\r", response.mid);
	}

	return false; 

}

bool XsensImu::getFilterProfile(uint8_t* filter, uint8_t* version) {
	XbusMessage getFilt = XbusMessage(XMID_ReqFilterProfile);
	XbusMessage response = doTransaction(&getFilt, (uint32_t)100);
	if (response.mid == XMID_ReqFilterProfileAck) {
		uint8_t const* rawData = response.data;
		rawData = XbusUtility_readU8(version, rawData);
		rawData = XbusUtility_readU8(filter, rawData);
		DEBUG_MESSAGE("Got getFilter Profile Ack. Fitler profile Version: %i, Filter: %i \n\r ", *version, *filter);
		return true; 
	} else {
		DEBUG_MESSAGE("No Filter Ack received.\n\r");
		return false; 
	}
}

bool XsensImu::setFilterProfile(uint8_t* filter) {
	uint8_t data[2] = {0, *filter};
	XbusMessage setFilt = XbusMessage(XMID_SetFilterProfile, data, 2);
	

	XbusMessage response = doTransaction(&setFilt, (uint32_t) 100);
	if (response.mid == XMID_ReqFilterProfileAck) {
		DEBUG_MESSAGE("Got setFilter Profile Ack. Filter profile: %i \n\r ", *filter);
	} else if (response.mid == XMID_Error) {
		handleError(&response);
		return false;
	} else {
		DEBUG_MESSAGE("No setFilter Ack received. Got message with id: 0x%x \n\r", response.mid);
		return false; 
	}
	return true; 
}


/* Initiates the manual gyro bias estimation. Only valid in measurement state. 
The success of the procedure is not indicated by the ACK, but by the state byte 
in the MTData2 message. 
/param in seconds: Gyro bias estimation procedure length in seconds
*/ 
bool XsensImu::manualGyroBiasEstimation(uint16_t* seconds) {
	XbusMessage mgbe = XbusMessage(XMID_SetNoRotation, seconds, 1);
	XbusMessage response = doTransaction(&mgbe);
	if (response.mid == XMID_SetNoRotationAck) {
		DEBUG_MESSAGE("Got MGBE Ack\n\r");
		return true; 
	}
	return false; 
}

bool XsensImu::enterconfig() {
	XbusMessage goConf = XbusMessage(XMID_GotoConfig);
  XbusMessage result  = doTransaction(&goConf, 1000
	);
  if (result.mid == XMID_GotoConfigAck) {
  	state = configuration;
  	DEBUG_MESSAGE("Enterconfig: Got response with MID 0x%x \n\r", result.mid);
  	return true; 
  } else {
  	DEBUG_MESSAGE("Enterconfig: Failed to enter config. Got mid 0x%u \n\r", result.mid);
  	return false; 
  }

}

bool XsensImu::getOptionFlags(uint32_t *optionFlags) {
	XbusMessage getOptFlags = XbusMessage(XMID_ReqOptionFlags);
	XbusMessage response = doTransaction(&getOptFlags, 500);

	if (response.mid == XMID_ReqOptionFlagsAck) {
		uint8_t const* rawData = response.data;
		XbusUtility_readU32(optionFlags, rawData);
		DEBUG_MESSAGE("Option flags received: 0x%lx \n\r", *optionFlags);
		return true; 
	}
	else {
		DEBUG_MESSAGE("No option flags received. Got 0x%x \n\r",response.mid);
		return false; 
	}
}

bool XsensImu::setOptionFlags(uint32_t* setFlags, uint32_t* clearFlags) {
	uint32_t data[2] = {*setFlags, *clearFlags};
	XbusMessage setOptFlags = XbusMessage(XMID_SetOptionFlags, data, 2);
	XbusMessage response = doTransaction(&setOptFlags, 500);

	if (response.mid == XMID_SetOptionFlagsAck) {
		DEBUG_MESSAGE("Option flags set successfully. \n\r");
		return true; 
	}
	else {
		DEBUG_MESSAGE("No setFlags ack received. Got: 0x%x \n\r", response.mid);
		return false; 
	}

}

bool XsensImu::getOutputConfig(uint32_t outputsettings) {
	XbusMessage getOutSet = XbusMessage(XMID_ReqOutputConfig);
	XbusMessage response = doTransaction(&getOutSet);
	if (response.mid == XMID_ReqOutputConfigAck) {
		
		uint8_t fields = response.length / 4;
		uint8_t const* rawData = response.data;

		for (int i = 0; i < fields; ++i)
		{
			uint16_t id, freq; 
			rawData = XbusUtility_readU16(&id, rawData);
			rawData = XbusUtility_readU16(&freq, rawData);
			DEBUG_MESSAGE("Field %s set at frequency %i \n\r", response.dataDescription(id), freq);
		}
		return true; 
	} else {
		return false; 
	}
}

bool XsensImu::setOutputConfig(OutputConfiguration* outputsettings, const uint8_t* length) {
	
	XbusMessage setOutSettings = XbusMessage(XMID_SetOutputConfig, outputsettings, *length);
	XbusMessage response = doTransaction(&setOutSettings, 1000);
	if (response.mid == XMID_SetOutputConfigAck) {
		uint8_t fields = response.length / 4;
		uint8_t const* rawData = response.data;

		for (int i = 0; i < fields; ++i)
		{
			uint16_t id, freq; 
			rawData = XbusUtility_readU16(&id, rawData);
			rawData = XbusUtility_readU16(&freq, rawData);
			DEBUG_MESSAGE("Field %s set at frequency %i \n\r", response.dataDescription(id), freq);
		}
		return true;
	} else {
		DEBUG_MESSAGE("No setOutputConfig ack received. Got: 0x%x \n\r", response.mid);
		return false;
	}



}

bool XsensImu::streamMeasurements() {
  XbusMessage goStream = XbusMessage(XMID_GotoMeasurement);
	XbusMessage result = doTransaction(&goStream);
	if (result.mid == XMID_GotoMeasurementAck) {
		return true; 
	} else {
		return false;
	}
}



