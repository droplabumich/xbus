#include "XBusParser.h"
#include "xbusdef.h"
#include "xbusutility.h"

#include "trace_debug.h"
#include "mbed.h"
XbusParser::XbusParser() {
	state = XBPS_Preamble;
}


/*!
 * \brief Parse an XMID_DeviceId message to extract the device ID value.

 * Replaces the raw Xbus message data with the device ID.
 */
/* void XbusParser::parseU32(uint8_t const* rawData)
{
	uint32_t deviceId; 

	XbusUtility_readU32(&deviceId, rawData);
	memcpy(currentMessage.data, &deviceId, sizeof(deviceId));

	//currentMessage.data[0] = deviceId;
	currentMessage.length = 1;
	
	
}*/



/*!
 * \brief Converts raw Xbus payload data to native structures if possible.
 *
 * Raw data payloads are converted to native data structures and the
 * message data pointer is changed to point to the native structure.
 * The raw data is automatically deallocated.
 */
/*void XbusParser::parseMessagePayload()
{
	uint8_t const* const rawData = currentMessage.data;
	switch (currentMessage.mid)
	{
		default:
			// Leave parsing and memory management to user code
			return;

		case XMID_DeviceId:
			parseU32(rawData);
			break;

		//case XMID_OutputConfig:
		//	parseOutputConfig(rawData);
		//	break;
		case XMID_FirmwareRev: 
			break;
		
	}

	
}*/

/*!
 * \brief Prepare for receiving a message payload.
 *
 * Requests a memory area to store the received data to using the
 * registered callbacks.
 */
void XbusParser::prepareForPayload()
{
	payloadReceived = 0;
	//currentMessage.data = parser->callbacks.allocateBuffer(parser->currentMessage.length);
}

/*!
 * \brief Parse a byte of data from a motion tracker.
 *
 * When a complete message is received the user will be notified by a call
 * to the handleMessage() callback function.
 */
void XbusParser::parseByte(const uint8_t byte)
{
	switch (state)
	{
		case XBPS_Preamble:
			if (byte == XBUS_PREAMBLE)
			{
				//DEBUG_MESSAGE("Got XBUS_PREAMBLE \n\r");
				checksum = 0;
				state = XBPS_BusId;
			}
			break;

		case XBPS_BusId:
			//DEBUG_MESSAGE("Got XBPS_BusId \n\r");

			checksum += byte;
			state = XBPS_MessageId;
			break;

		case XBPS_MessageId:
			//DEBUG_MESSAGE("Got XBPS_MessageId \n\r");
			checksum += byte;
			currentMessage.mid = (enum XsMessageId)byte;
			state = XBPS_Length;
			break;

		case XBPS_Length:
			//DEBUG_MESSAGE("Got XBPS_Length \n\r");

			checksum += byte;
			if (byte == XBUS_NO_PAYLOAD)
			{
				currentMessage.length = byte;
				state = XBPS_Checksum;
			}
			else if (byte < XBUS_EXTENDED_LENGTH)
			{
				currentMessage.length = byte;
				prepareForPayload();
				state = XBPS_Payload;
			}
			else
			{
				state = XBPS_ExtendedLengthMsb;
			}
			break;

		case XBPS_ExtendedLengthMsb:
			//DEBUG_MESSAGE("Got XBPS_ExtendedLengthMsb \n\r");

			checksum += byte;
			currentMessage.length = ((uint16_t)byte) << 8;
			state = XBPS_ExtendedLengthLsb;
			break;

		case XBPS_ExtendedLengthLsb:
			//DEBUG_MESSAGE("Got XBPS_ExtendedLengthLsb \n\r");

			checksum += byte;
			currentMessage.length |= byte;
			prepareForPayload();
			state = XBPS_Payload;
			break;

		case XBPS_Payload:

			checksum += byte;
			if (currentMessage.data)
			{
				currentMessage.data[payloadReceived] = byte;
			}
			if (++payloadReceived == currentMessage.length)
			{
				state = XBPS_Checksum;
			}
			break;

		case XBPS_Checksum:
			//DEBUG_MESSAGE("Got XBPS_Checksum \n\r");

			checksum += byte;
			if (checksum == 0) // && ((currentMessage.length == 0) || currentMessage.data))
			{
				//DEBUG_MESSAGE("Checksum passed! \n\r");
				//parseMessagePayload();
				handleMessage();
			}
			/*else if (currentMessage.data)
			{
				parser->callbacks.deallocateBuffer(parser->currentMessage.data);
			}*/
			state = XBPS_Preamble;
			break;
		default: // In case something happens and the state gets messed up, return to the beginning
			state = XBPS_Preamble;
			break;
	}
}

XbusParserState XbusParser::getState() {
	return state;
}




