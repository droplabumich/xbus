#ifndef __XSENSCONFIG_H
#define __XSENSCONFIG_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include "xbusutility.h"
#include "XbusMessage.h"

#define DisableAutoStoreFlag 0 
#define DisableAutoMeasurement 1 
#define EnableBeidou 2
#define EnableAHS 4 
#define EnableConfigurableBusId 5 
#define EnableInRunCompassCalibration 6   


enum XsFilterId {
	XsFilter_MTI_10_General = 39,
	XsFilter_MTI_10_HighMagDep = 40,
	XsFilter_MTI_10_Dynamic = 41,
	XsFilter_MTI_10_LowMagDep = 42,
	XsFilter_MTI_10_VRUGeneral = 43,
};

/*!
 * \brief Output configuration structure.
 */
struct OutputConfiguration
{
	/*! \brief Data type of the output. */
	uint16_t dtype;
	/*!
	 * \brief The output frequency in Hz, or 65535 if the value should be
	 * included in every data message.
	 */
	uint16_t freq;
};



class  ImuOptions 
{
 public:
	ImuOptions(); 
	ImuOptions(uint32_t flags, 
			   uint16_t attitude_rate, uint16_t acceleration_rate, uint16_t rateofturn_rate, uint16_t temperature_rate, 
			   XsFilterId filter);
	ImuOptions(bool disAutoStore, bool disAutoMeas, bool enbleBeidou, bool enbleAHS, bool enbleConfBusID, bool IRCC, 
			   uint16_t attitude_rate, uint16_t acceleration_rate, uint16_t rateofturn_rate, uint16_t temperature_rate, 
			   XsFilterId filter);
	
	uint32_t toFlags(); // Convert the flag settings to uint32_t number
	uint32_t getClearFlags( uint32_t*  setFlags); // Compute the flags to be cleared 

	bool getOutputConfig(OutputConfiguration* outputConfig, const uint8_t* const length); // Generate the outputconfig array to be send to IMU for config

	XsFilterId getFilter() {return _filter;};

 private:
	// Configuration flags 
	bool disableAutoStore;
	bool disableAutoMeasurement; 
	bool enableBeidou; 
	bool enableAHS;
	bool enableConfigurableBusId; 
	bool enableInRunCompassCalibration; 

	// Output settings: Specify the output frequencies of each field
	uint16_t attitude_rate, acceleration_rate, rateofturn_rate, temperature_rate;

	// Filter settings: Type of filter 
	XsFilterId _filter;
};



#endif